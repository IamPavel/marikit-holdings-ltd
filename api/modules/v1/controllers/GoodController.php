<?php

namespace api\modules\v1\controllers;

use yii\web\Controller;

class GoodController extends Controller
{
    public function actionIndex()
    {
        return array (
            'Error' => '',
            'Id' => 0,
            'Success' => true,
            'Value' =>
                array (
                    'Goods' =>
                        array (
                            0 =>
                                array (
                                    'B' => false,
                                    'C' => 158,
                                    'CV' => NULL,
                                    'G' => 1,
                                    'P' => 1,
                                    'Pl' => NULL,
                                    'T' => 1,
                                ),
                            1 =>
                                array (
                                    'B' => false,
                                    'C' => 197,
                                    'CV' => NULL,
                                    'G' => 1,
                                    'P' => 99,
                                    'Pl' => NULL,
                                    'T' => 2,
                                ),
                            2 =>
                                array (
                                    'B' => false,
                                    'C' => 18,
                                    'CV' => NULL,
                                    'G' => 1,
                                    'P' => 31,
                                    'Pl' => NULL,
                                    'T' => 3,
                                ),
                            3 =>
                                array (
                                    'B' => false,
                                    'C' => 2.14,
                                    'CV' => NULL,
                                    'G' => 2,
                                    'P' => 15,
                                    'Pl' => NULL,
                                    'T' => 8,
                                ),
                            4 =>
                                array (
                                    'B' => false,
                                    'C' => 1.52,
                                    'CV' => NULL,
                                    'G' => 2,
                                    'P' => 76,
                                    'Pl' => NULL,
                                    'T' => 86,
                                ),
                            5 =>
                                array (
                                    'B' => false,
                                    'C' => 5.5,
                                    'CV' => NULL,
                                    'G' => 2,
                                    'P' => 100,
                                    'Pl' => NULL,
                                    'T' => 126,
                                ),
                            6 =>
                                array (
                                    'B' => false,
                                    'C' => 2.71,
                                    'CV' => NULL,
                                    'G' => 5,
                                    'P' => 51,
                                    'Pl' => NULL,
                                    'T' => 184,
                                ),
                            7 =>
                                array (
                                    'B' => false,
                                    'C' => 3.95,
                                    'CV' => NULL,
                                    'G' => 5,
                                    'P' => 2,
                                    'Pl' => NULL,
                                    'T' => 185,
                                ),
                            8 =>
                                array (
                                    'B' => false,
                                    'C' => 1.21,
                                    'CV' => NULL,
                                    'G' => 10,
                                    'P' => 51,
                                    'Pl' => NULL,
                                    'T' => 194,
                                ),
                            9 =>
                                array (
                                    'B' => false,
                                    'C' => 1.179,
                                    'CV' => NULL,
                                    'G' => 15,
                                    'P' => 55,
                                    'Pl' => NULL,
                                    'T' => 12,
                                ),
                            10 =>
                                array (
                                    'B' => false,
                                    'C' => 1.55,
                                    'CV' => NULL,
                                    'G' => 15,
                                    'P' => 64,
                                    'Pl' => NULL,
                                    'T' => 63,
                                ),
                            11 =>
                                array (
                                    'B' => false,
                                    'C' => 1.55,
                                    'CV' => NULL,
                                    'G' => 15,
                                    'P' => 77,
                                    'Pl' => NULL,
                                    'T' => 64,
                                ),
                        ),
                ),
        );
    }
}